/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saharat.mideterm;

/**
 *
 * @author Sakonsupa
 */
public class Product {
    private String id;
    private String name;
    private String brand;
    private double price;
    private int amount;

    public Product(String id, String name, String brand, double price, int amount) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
    }

    public void setId(String id) {
        this.id = id;
    }

   

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public double getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    } 
    @Override
    public String toString() {
        return  "       " + id + "                           " + name 
          + "                                            " + brand 
          + "                                                " + price 
          + "                                    " + amount ;
    }
}
